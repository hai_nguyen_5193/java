import java.io.File;
import java.util.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.geom.Line2D;
import javax.imageio.*;
import com.jogamp.opengl.util.awt.*;
import static java.lang.Math.*;

import com.jogamp.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.texture.*;
import com.jogamp.opengl.util.texture.awt.*;

import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.texture.*;
import com.jogamp.opengl.util.texture.awt.*;

/**
 * A class representing the scene of the Room
 * @author Phan Trung Hai Nguyen
 * Borrowed some codes from Steve's (i.e. loadTexture, setMaterial...)
 */
public class MyRestaurantScene {

	private GLU glu = new GLU();
	private GLUT glut = new GLUT();

	private final int ROOM_WIDTH = 60;  
	private final int ROOM_HEIGHT = 30;	
	
	private int frameNumber = 0;
	private boolean objectsOn = true;
	private boolean roomLightOn = true;
	private boolean robotViewOn = false;
	private boolean ceilingLightsOn = true;
	
	private Mesh meshWall1, meshWall2, meshWall3, 
					meshWall4, meshFloor, meshCeiling, meshTableTop;
	private Render wall1, wall2, wall3, wall4, floor, ceiling, tableTop;
	
	private BufferedImage textureImage;	
	private TextureCollection wall1Tex;
	private int currentTexture = 0;  

	private int canvaswidth=0, canvasheight=0;

	private Light roomLight, eyeRLight, eyeLLight,
						ceilingSpotlight1, ceilingSpotlight2;	
	
	private Camera camera;
	private AnimationScene animationScene;
	private Robot robot;
	private Axes axes;  

	//textures for floor, wall and ceiling
	private Texture floorTex, ceilingTex, tableTex, 
					wall2Tex, wall3Tex, wall4Tex;

	/**
	 * Constructor to instantiate Scene class
	 * @param gl A GL2 object
	 * @param camera a Camera object	
	 */
	public MyRestaurantScene(GL2 gl, Camera camera) {
		animationScene = new AnimationScene();
		robot = new Robot(gl, animationScene,3.0,1.5,true);
		
		roomLight = new Light(GL2.GL_LIGHT0, new float[]{0.0f,25.5f,0.0f, 1.0f}); 

		//spotlights for two eyes
		float[] position = {0,0,0,1};
		eyeLLight = new Light(GL2.GL_LIGHT1, position);
		eyeRLight = new Light(GL2.GL_LIGHT2, position);
		float[] direction = {0,-1,3};  //pointing down the floor
		eyeLLight.makeSpotlight(direction,10f);   
		eyeRLight.makeSpotlight(direction,10f);   
		
		//spotlights for two ceiling spotlights		
		ceilingSpotlight1 = new Light(GL2.GL_LIGHT3, position);
		ceilingSpotlight2 = new Light(GL2.GL_LIGHT4, position);
		float[] direction1 = {0,0,1};  
		ceilingSpotlight1.makeSpotlight(direction1,10f);   
		ceilingSpotlight2.makeSpotlight(direction1,10f);   

		this.camera = camera;
		axes = new Axes(2.2,1.8,1.6);		
		
		createRenderObjects(gl);  			
		
		//texture collection for animation
		wall1Tex = new TextureCollection();
        wall1Tex.add(gl, "an0.jpg");
		wall1Tex.add(gl, "an1.jpg");
		wall1Tex.add(gl, "an2.jpg");
		wall1Tex.add(gl, "an3.jpg"); 
		wall1Tex.add(gl, "an4.jpg"); 
		wall1Tex.add(gl, "an5.jpg"); 		
	}	
	
	/**
	 * create all redering objects
	 * @param gl A GL2 object	 
	 */
	private void createRenderObjects(GL2 gl) { 		
		//load all textures		
		floorTex = loadTexture(gl, "floorTex.jpg");
		ceilingTex = loadTexture(gl, "ceilingTex.jpg");
		tableTex = loadTexture(gl, "tableTex.jpg");	
		wall2Tex = loadTexture(gl, "wall2Tex.jpg");
		wall3Tex = loadTexture(gl, "wall3Tex.jpg");
		wall4Tex = loadTexture(gl, "wall4Tex.jpg");
		
		//floor
		meshFloor = ProceduralMeshFactory.createPlane(ROOM_WIDTH,ROOM_WIDTH,100,100,9,9);  
		floor = new Render(meshFloor, floorTex);    
		floor.initialiseDisplayList(gl, true);
		
		//wall 1 (animated)
		meshWall1 = ProceduralMeshFactory.createPlane(ROOM_WIDTH,ROOM_HEIGHT,50,50,1,1);  	
		
		//wall 2
		meshWall2 = ProceduralMeshFactory.createPlane(ROOM_WIDTH,ROOM_HEIGHT,50,50,1,1);  
		wall2 = new Render(meshWall2, wall2Tex);    
		wall2.initialiseDisplayList(gl, true);
  		
		//wall 3
		meshWall3 = ProceduralMeshFactory.createPlane(ROOM_WIDTH,ROOM_HEIGHT,50,50,1,1);  
		wall3 = new Render(meshWall3, wall3Tex);    
		wall3.initialiseDisplayList(gl, true);
		
		//wall 4
		meshWall4 = ProceduralMeshFactory.createPlane(ROOM_WIDTH,ROOM_HEIGHT,50,50,1,1); 
		wall4 = new Render(meshWall4, wall4Tex);   
		wall4.initialiseDisplayList(gl, true);
		
		//ceiling
		meshCeiling = ProceduralMeshFactory.createPlane(ROOM_WIDTH,ROOM_WIDTH,10,10,10,10);  
		ceiling = new Render(meshCeiling, ceilingTex);    
		ceiling.initialiseDisplayList(gl, true); 

		//table top
		meshTableTop = ProceduralMeshFactory.createHardCube(8,8,8);
		tableTop = new Render(meshTableTop, tableTex);    
		tableTop.initialiseDisplayList(gl, true);			
	}
	
	/**
	 * Returns a Texture object
	 * Borrowed from Steve's
	 * @param gl A GL2 object
	 * @param filename texture file
	 * @return a Texture object 
	 */
	private Texture loadTexture(GL2 gl, String filename) {
		Texture tex = null;	   
		try {
			File f = new File(filename);
			
			BufferedImage img = ImageIO.read(f); 
			ImageUtil.flipImageVertically(img);
			tex = AWTTextureIO.newTexture(GLProfile.getDefault(), img, false);

			tex.setTexParameteri(gl, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_NEAREST);
			tex.setTexParameteri(gl, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_NEAREST);			
		
			tex.setTexParameteri(gl, GL2.GL_TEXTURE_WRAP_S, GL2.GL_REPEAT);
			tex.setTexParameteri(gl, GL2.GL_TEXTURE_WRAP_T, GL2.GL_REPEAT);			
		}catch(Exception e) {
			System.out.println("Error loading texture " + filename); 
		}
		return tex;
	}   
    
	private void debugTexture(Texture tex) {
		System.out.println(tex);
		try {
			TextureIO.write(tex, new File("debug_tex.jpg"));
		}catch(Exception e) {
			System.out.println("Error saving texture "); 
		}
	}
	   
	public void setCanvasSize(int w, int h) {
		canvaswidth=w;
		canvasheight=h;
	}

	public void setObjectsDisplay(boolean b) {
		objectsOn = b;
	}
	
	public void setRoomLight(boolean b) {
		roomLightOn = b;
	}
	
	public void setCeilingLights(boolean b){
		ceilingLightsOn = b;
	}
	
	public void setEyeLight(boolean b) {
		robot.setEyeLight(b);
	}	
	
	public void setRobotView(boolean b){
		robotViewOn = b;
	}

	public Light getLight() {
		return roomLight;
	}
	
	public Light getCeilingLight1(){
		return ceilingSpotlight1;
	}
	
	public Light getCeilingLight2(){
		return ceilingSpotlight2;
	}
	
	public Light getEyeLLight() {
		return eyeLLight;
	}
	
	public Light getEyeRLight() {
		return eyeRLight;
	}

	public Axes getAxes() {
		return axes;
	}	
	 
	public void reset() {
		animationScene.reset();		
		frameNumber = 0;		
		setObjectsDisplay(true);
	}	
	
	public void startAnimation() {
		animationScene.startAnimation();
	}

	public void pauseAnimation() {
		animationScene.pauseAnimation();
	}
	
	/**
	 * Update the frameNum for animated wall and scene
	 * @param gl A GL2 object	
	 */
	public void update(GL2 gl) {
		animationScene.update();		
		frameNumber = (frameNumber+1)%60;
		if (frameNumber < 10) currentTexture = 0;
		else if (frameNumber < 20) currentTexture = 1;
		else if (frameNumber < 30) currentTexture = 2;
		else if (frameNumber < 40) currentTexture = 3;
		else if (frameNumber < 50) currentTexture = 4;
		else if (frameNumber < 60) currentTexture = 5;
	}

	//room light
	private void doRoomLight(GL2 gl) {
		gl.glPushMatrix();		
			roomLight.use(gl, glut, true);
		gl.glPopMatrix();
	}

	//robot left eye spotlight
	private void doEyeLLight(GL2 gl) {
		gl.glPushMatrix();			
			eyeLLight.use(gl, glut, true);			
		gl.glPopMatrix();
	}  
	
	//robot right eye spotlight
	private void doEyeRLight(GL2 gl) {
		gl.glPushMatrix();			
			eyeRLight.use(gl, glut, true);			
		gl.glPopMatrix();
	}  
	
	//ceiling spotlight 1 
	private void doCeilingSpotlight1(GL2 gl) {
		gl.glPushMatrix();			
			ceilingSpotlight1.use(gl, glut, true);			
		gl.glPopMatrix();
	}  
	
	//ceiling spotlight 2
	private void doCeilingSpotlight2(GL2 gl) {
		gl.glPushMatrix();			
			ceilingSpotlight2.use(gl, glut, true);			
		gl.glPopMatrix();
	}  
  
    /**
	 * Render the scene
	 * @param gl A GL2 object	
	 */
	public void render(GL2 gl) {
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT|GL2.GL_DEPTH_BUFFER_BIT);		
		gl.glLoadIdentity();
		
		//set camera view
		if (robotViewOn) robot.setRobotView(); 
		else camera.view(glu); 
		
		doRoomLight(gl);   

		//render 2 robot eye spotlights
		renderRobotEyes(gl);

		//spotlights for ceiling
		renderSpotlightsForCeiling(gl);

		if (axes.getSwitchedOn())  axes.display(gl, glut);
		if (objectsOn)  displayObjects(gl);		
	} 	
	
		
	/**
	 * Render robot's eyes
	 * @param gl A GL2 object	
	 */
	private void renderRobotEyes(GL2 gl){
		gl.glPushMatrix();
			robot.transformRobot();
			gl.glPushMatrix();  						
				robot.transformSpotLightToRobotEye(10);
				doEyeLLight(gl);
			gl.glPopMatrix();
			gl.glPushMatrix();  					
				robot.transformSpotLightToRobotEye(-10);
				doEyeRLight(gl);
			gl.glPopMatrix();		
		gl.glPopMatrix();	
	}
	
	/**
	 * Render spotlights for ceiling
	 * @param gl A GL2 object	
	 */
	private void renderSpotlightsForCeiling(GL2 gl){
		gl.glPushMatrix();
			gl.glPushMatrix();
				transformSpotlightToCeiling(gl,20,ROOM_HEIGHT,-20,-45);
				transformSpotlightToCeilingLight(gl);
				doCeilingSpotlight1(gl);
			gl.glPopMatrix();
			gl.glPushMatrix();
				transformSpotlightToCeiling(gl,-20,ROOM_HEIGHT,20,135);
				transformSpotlightToCeilingLight(gl);
				doCeilingSpotlight2(gl);
			gl.glPopMatrix();		
		gl.glPopMatrix();
	}
  
	/**
	 * Display all objects
	 * @param gl A GL2 object	
	 */
	private void displayObjects(GL2 gl) {  
		floor.renderDisplayList(gl);		
		
		//change texture for wall 1
		Texture currentTex = wall1Tex.get(currentTexture);
		wall1 = new Render(meshWall1,currentTex);  			
		
		displayWalls(gl);		
		displayCeiling(gl);	
		robot.displayRobot();		
		displayTables(gl);
		
		gl.glPushMatrix();  //world lights
			gl.glTranslated(0,ROOM_HEIGHT,0);
			drawRoomLight(gl);
		gl.glPopMatrix();
		
		gl.glPushMatrix();  //ceiling spotlight 1
		    transformSpotlightToCeiling(gl,20,ROOM_HEIGHT,-20,-45);
			drawCeilingSpotlight(gl);
		gl.glPopMatrix();
		
		gl.glPushMatrix();  //ceiling spotlight 2
		    transformSpotlightToCeiling(gl,-20,ROOM_HEIGHT,20,135);
			drawCeilingSpotlight(gl);
		gl.glPopMatrix();
		
		gl.glPushMatrix();  //projector
			gl.glTranslated(0,ROOM_HEIGHT,-10);
			gl.glRotated(180,0,1,0);
			drawProjector(gl);
		gl.glPopMatrix();		
		drawChristmasTree(gl);
	}	
	
	/**
	 * Display ceiling
	 * @param gl A GL2 object	
	 */
	private void displayCeiling(GL2 gl){
		gl.glPushMatrix(); 		
			gl.glTranslated(0,ROOM_HEIGHT,0);
			gl.glRotated(180,0,0,1);
			ceiling.renderDisplayList(gl);	
		gl.glPopMatrix();
	}
	
	/**
	 * Display all walls
	 * @param gl A GL2 object	
	 */
	private void displayWalls(GL2 gl){
		gl.glPushMatrix();  //wall 1
			gl.glTranslated(0,ROOM_HEIGHT/2,-ROOM_WIDTH/2);
			gl.glRotated(90,1,0,0);		
			wall1.renderImmediateMode(gl,true);
		gl.glPopMatrix();
	
		gl.glPushMatrix();  //wall 2
			gl.glTranslated(-ROOM_WIDTH/2,ROOM_HEIGHT/2, 0);
			gl.glRotated(90,1,0,0);	
			gl.glRotated(-90,0,0,1);
			wall2.renderDisplayList(gl);		
		gl.glPopMatrix();
	
		gl.glPushMatrix();  //wall 3
			gl.glTranslated(0, ROOM_HEIGHT/2, ROOM_WIDTH/2);
			gl.glRotated(180,0,1,0);
			gl.glRotated(90,1,0,0);
			wall3.renderDisplayList(gl);	
		gl.glPopMatrix();
	
		gl.glPushMatrix();  //wall 4
			gl.glTranslated(ROOM_WIDTH/2, ROOM_HEIGHT/2, 0);
			gl.glRotated(90,1,0,0);	
			gl.glRotated(90,0,0,1);
			wall4.renderDisplayList(gl);		
		gl.glPopMatrix();		
	}		
	
	/**
	 * Display all tables
	 * @param gl A GL2 object	
	 */
	private void displayTables(GL2 gl){
		double item1 = animationScene.getParam(animationScene.ITEM1_PARAM);
		double item2 = animationScene.getParam(animationScene.ITEM2_PARAM);
		double item3 = animationScene.getParam(animationScene.ITEM3_PARAM);
		
		drawTable(gl,16,16,item1,1.0,1.0f,0.0f,0.0f);
		drawTable(gl,-16,16,item2,0.5,0.76f,0.79f,0.0f);
		drawTable(gl,-16,-16,item3,0.5,0.75f,0.18f,0.71f);
	}		
	
	/**
	 * Transform spotlights to ceiling light position
	 * @param gl A GL2 object	
	 */
	private void transformSpotlightToCeilingLight(GL2 gl){			
		double theta = animationScene.getParam(animationScene.CEILING_LIGHT_PARAM);
		
		double r = 1.5*sin(PI/4);
		double dx1 = -r*sin(toRadians(theta));
		double dy1 = r*cos(toRadians(theta));
		double dz1 = 1.5*cos(toRadians(45));
		
		double dx = dx1;
		double dy = dy1*cos(toRadians(20))-dz1*sin(toRadians(20))-0.56*sin(toRadians(20));
		double dz = 0.56*cos(toRadians(20))+0.15+dz1*cos(toRadians(20))+dy1*sin(toRadians(20));
		
		gl.glRotated(90,1,0,0);
		gl.glTranslated(dx,dy,dz);		
		gl.glRotated(theta,0,0,1);
		gl.glRotated(-25,1,0,0);
	}
	
	/**
	 * Transform spotlights to ceiling
	 * @param gl A GL2 object	
	 * @param x x-coordinate
	 * @param y y-coordinate
	 * @param z z-coordinate
	 * @param theta 
	 */
	private void transformSpotlightToCeiling(GL2 gl, double x, double y, double z, double theta){
		gl.glTranslated(x,y,z);
		gl.glRotated(theta,0,1,0);
	}	
	
	/**
	 * Draw christmas tree and gifts
	 * @param gl A GL2 object	
	 */
	private void drawChristmasTree(GL2 gl){	
		
		GLUquadric quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
				
		gl.glPushMatrix();
			gl.glRotated(-90,1,0,0);
			setMaterial(gl,0.46f,0.45f,0.45f);
			glu.gluCylinder(quadric,5.0f,5.0f,0.5f,30,30);			
			gl.glPushMatrix();
				gl.glTranslated(0,0,0.5);
				glu.gluDisk(quadric,0.0,5.0,30,30);	
				gl.glPushMatrix();
					drawGift(gl,0.89f,0.27f,0.27f,-2,-2.5,0.8,2f); //gift 1
					drawGift(gl,0.92f,0.57f,0.0f,2,-2.5,0.8,1.5f); //gift 2
					drawGift(gl,0.74f,0.27f,0.65f,2,1.5,0.8,1.5f); //gift 3				
					gl.glPushMatrix();
						setMaterial(gl,0.73f,0.56f,0.25f);
						glu.gluCylinder(quadric,1.5f,0.5f,4.0f,20,20);
						gl.glPushMatrix();
							setMaterial(gl,0.05f,0.51f,0.06f);
							
							//animating emission for christmas tree
							float a = (float) animationScene.getParam(animationScene.CHRISTMAS_TREE_LIGHT_PARAM);
							setEmission(gl,a,a,a);		
							
							gl.glTranslated(0,0,4);
							gl.glPushMatrix();
								gl.glRotated(180,1,0,0);
								glu.gluDisk(quadric,0.0,5.0,30,30);
							gl.glPopMatrix();
							glu.gluCylinder(quadric,5.0f,0.0f,5.0f,20,20); //bottom layer
							gl.glPushMatrix();
								gl.glTranslated(0,0,3.5);
								gl.glPushMatrix();
									gl.glRotated(180,1,0,0);
									glu.gluDisk(quadric,0.0,3.5,30,30);
								gl.glPopMatrix();
								glu.gluCylinder(quadric,3.5f,0.0f,4.0f,20,20);  //mid layer
								gl.glPushMatrix();
									gl.glTranslated(0,0,3);
									gl.glPushMatrix();
										gl.glRotated(180,1,0,0);
										glu.gluDisk(quadric,0.0,2.0,30,30);
									gl.glPopMatrix();
									glu.gluCylinder(quadric,2.0f,0.0f,3.0f,20,20);  //top layer
								gl.glPopMatrix();
							gl.glPopMatrix();
							setEmission(gl,0.0f,0.0f,0.0f);
						gl.glPopMatrix();						
					gl.glPopMatrix();			
				gl.glPopMatrix();
			gl.glPopMatrix();			
		gl.glPopMatrix();
		
		glu.gluDeleteQuadric(quadric);	
	}
	
	/**
	 * Draw gift positioned below the christmas tree
	 * @param gl A GL2 object
	 * @param mat1,mat2,mat3 material properties
	 * @param x,y,z position of gift
	 * @param size size of the gift (cube edge)
	 */
	private void drawGift(GL2 gl, float mat1, float mat2, float mat3,
							double x, double y, double z, float size){
		gl.glPushMatrix(); 
			setMaterial(gl,mat1,mat2,mat3);
			gl.glTranslated(x,y,z);
			glut.glutSolidCube(size);
		gl.glPopMatrix();
	}
	
	/**
	 * Draw a projector pointing to wall (for wall animation)
	 * @param gl A GL2 object	
	 */
	private void drawProjector(GL2 gl){
		GLUquadric quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);
		
		gl.glPushMatrix();
			gl.glRotated(90,1,0,0);
			gl.glPushMatrix();
				gl.glRotated(180,1,0,0);
				glu.gluDisk(quadric,0.0f,0.1f,5,5);
			gl.glPopMatrix();
			glu.gluCylinder(quadric,0.1f,0.1f,2.5f,10,10);
			gl.glPushMatrix();
				gl.glTranslated(0,0,3.1);
				gl.glRotated(15,1,0,0);
				gl.glPushMatrix();
					gl.glScaled(1,1,0.4);
					glut.glutSolidCube(3);
				gl.glPopMatrix();
				gl.glPushMatrix();
					gl.glTranslated(0,1.5,0);
					gl.glRotated(-90,1,0,0);
					glu.gluCylinder(quadric,0.3f,0.4f,0.5f,10,10);
					gl.glPushMatrix();
						gl.glTranslated(0,0,0.25);
						setEmission(gl,0.8f,0.8f,0.8f);
						glu.gluDisk(quadric,0.0f,0.35f,10,10);
						setEmission(gl,0.0f,0.0f,0.0f);
					gl.glPopMatrix();
				gl.glPopMatrix();
			gl.glPopMatrix();
		gl.glPopMatrix();
		
		glu.gluDeleteQuadric(quadric);		
	}
	
	/**
	 * Draw room light (world light)
	 * @param gl A GL2 object	
	 */
	private void drawRoomLight(GL2 gl){
		setMaterial(gl,0.658f,0.658f,0.658f);	
		
		GLUquadric quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);	
		
		gl.glPushMatrix();
			gl.glRotated(90,1,0,0);			
			gl.glPushMatrix();
				gl.glRotated(180,1,0,0);
				glu.gluDisk(quadric,0.0,1.0,15,15);
			gl.glPopMatrix();
			glu.gluCylinder(quadric, 1.0f, 0.2f, 2.5f, 15, 15);
			gl.glPushMatrix();
				gl.glTranslated(0,0,4.5);
				gl.glRotated(-90,1,0,0);
				if (roomLightOn) setEmission(gl,0.8f,0.8f,0.8f);		
				glut.glutSolidSphere(2,20,20);
				for (int i=0; i<5; i++){
					double theta = i*(360/5.0);
					gl.glPushMatrix();
						gl.glRotated(theta,0,1,0);
						gl.glTranslated(0,0,2);
						setEmission(gl,0.0f,0.0f,0.0f);
						glu.gluCylinder(quadric, 0.5f, 0.3f, 1, 15, 15);	
						gl.glPushMatrix();
							gl.glTranslated(0,0,2);
							if (roomLightOn) setEmission(gl,0.8f,0.8f,0.8f);
							glut.glutSolidSphere(1,15,15);
						gl.glPopMatrix();
					gl.glPopMatrix();
				}
			gl.glPopMatrix();
		gl.glPopMatrix();
		
		glu.gluDeleteQuadric(quadric);	
	}	
	
	/**
	 * Draw celing spotlights
	 * @param gl A GL2 object	
	 */
	private void drawCeilingSpotlight(GL2 gl){
		setEmission(gl,0.0f,0.0f,0.0f);
		GLUquadric quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);	
		
		gl.glPushMatrix();			
			gl.glRotated(90,1,0,0);
			gl.glPushMatrix();
				gl.glRotated(180,1,0,0);
				glu.gluDisk(quadric,0.0,0.5,15,15);
			gl.glPopMatrix();
			glu.gluCylinder(quadric,0.5f,0.5f,0.15f,15,15);				
			gl.glPushMatrix();
				gl.glTranslated(0,0,0.15);
				glu.gluDisk(quadric,0.0,0.5,15,15);
			gl.glPopMatrix();
			gl.glPushMatrix();
				gl.glTranslated(0,0,0.15);
				gl.glRotated(20,1,0,0);
				gl.glPushMatrix();
					gl.glTranslated(0,0,0.28);
					gl.glScaled(0.2,0.2,1);
					glut.glutSolidSphere(0.28,15,15);
				gl.glPopMatrix();
				gl.glPushMatrix();
					gl.glTranslated(0,0,0.5);
					
					//swing the ceiling spotlights
					double theta = animationScene.getParam(animationScene.CEILING_LIGHT_PARAM);
					gl.glRotated(theta,0,0,1);
					
					gl.glRotated(-45,1,0,0); 
					gl.glPushMatrix();			
						gl.glRotated(180,1,0,0);
						glu.gluDisk(quadric,0.0,0.3,15,15);
					gl.glPopMatrix();	
					glu.gluCylinder(quadric,0.3f,0.5f,1.5f,15,15);					
					gl.glPushMatrix();
						gl.glTranslated(0,0,1.5);
						glu.gluCylinder(quadric,0.5f,0.7f,0.5f,15,15);
					gl.glPopMatrix();
					gl.glPushMatrix();
						gl.glTranslated(0,0,1.5);
						if (ceilingLightsOn) setEmission(gl,0.8f,0.8f,0.8f);
						glut.glutSolidSphere(0.45,15,15);
						setEmission(gl,0.0f,0.0f,0.0f);
					gl.glPopMatrix();
				gl.glPopMatrix();
			gl.glPopMatrix();
		gl.glPopMatrix();
		
		glu.gluDeleteQuadric(quadric);	
	}	
	
	/**
	 * Draw a table
	 * @param gl A GL2 object	
	 * @param x x-coordinate
	 * @param z z-coordinate
	 * @param item state of item on tray
	 * @param itemSize size of item on tray
	 * @param r red component
	 * @param g green component
	 * @param b blue component
	 */
	private void drawTable(GL2 gl, double x, double z, double item, double itemSize, float r, float g, float b){					
		gl.glPushMatrix();
			gl.glTranslated(x,0,z);
			gl.glPushMatrix();  //table top
				gl.glTranslated(0,4,0);
				gl.glScaled(1,0.08,1);
				gl.glTranslated(0,4,0);
				tableTop.renderDisplayList(gl);
			gl.glPopMatrix();
			if (item == 0.0){
				gl.glPushMatrix(); //item on table
					setMaterial(gl,r,g,b);	
					gl.glTranslated(0.0,4.64+itemSize/2.0,0.0);
					glut.glutSolidCube((float) itemSize); 					
				gl.glPopMatrix();
			}			
			gl.glPushMatrix(); //4 legs
				setMaterial(gl,0.65f,0.50f,0.39f);	
				drawTableLeg(gl,-3,3);
				drawTableLeg(gl,3,3);
				drawTableLeg(gl,3,-3);
				drawTableLeg(gl,-3,-3);				
			gl.glPopMatrix();			
		gl.glPopMatrix();
	}
	
	/**
	 * Draw table leg
	 * @param gl A GL2 object
	 * @param x x-coordinate
	 * @param z z-coordinate
	 */
	private void drawTableLeg(GL2 gl, double x, double z){
		gl.glPushMatrix();
			gl.glTranslated(x,2,z);
			gl.glScaled(0.1, 1, 0.1);
			glut.glutSolidCube(4);
		gl.glPopMatrix();
	}
	
	/**
	 * set material properties for object
	 * @param gl A GL2 object
	 * @param r red coponent
	 * @param g glue component
	 * @param b blue component
	 */
	private void setMaterial(GL2 gl, float r, float g, float b) {
		float[] matAmbientDiffuse = {r,g,b, 1.0f};
		float[] matSpecular = {0.5f,0.5f,0.5f, 1.0f};
		float[] matShininess = {16.0f};
		float[] matEmission = {0.0f, 0.0f, 0.0f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, matAmbientDiffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
	}
	
	/**
	 * Set emission property for object
	 * @param gl A GL2 object
	 * @param a
	 * @param b
	 * @param c
	 */
	private void setEmission(GL2 gl, float a, float b, float c){
		float[] emission = {a, b, c, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, emission, 0);
	}
}