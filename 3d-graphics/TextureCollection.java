import java.io.File;
import java.util.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.geom.Line2D;
import javax.imageio.*;
import com.jogamp.opengl.util.awt.*;

import com.jogamp.opengl.*;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.util.texture.*;
import com.jogamp.opengl.util.texture.awt.*;

import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.texture.*;
import com.jogamp.opengl.util.texture.awt.*;

class TextureCollection {
  
	private ArrayList<Texture> textures;
 
	public TextureCollection() {
		textures = new ArrayList<Texture>();
	}
  
	public synchronized void add(GL2 gl, String filename) {
		System.out.println(filename); 
		Texture tex = null;	
		try {
			File f = new File(filename);
			BufferedImage textureImage = ImageIO.read(f); // read file into BufferedImage
			ImageUtil.flipImageVertically(textureImage);
			tex = AWTTextureIO.newTexture(GLProfile.getDefault(), textureImage, false);
			
			tex.setTexParameteri(gl, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
			tex.setTexParameteri(gl, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
			System.out.println(tex);
      
		}
		catch(Exception e) {
			System.out.println("Error loading texture " + filename); 
		}
		textures.add(tex);
	}
  
	public Texture get(int i) {
		return textures.get(i);
	}
}
