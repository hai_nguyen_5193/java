import java.awt.*;
import java.awt.event.*;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * A class representing the restaurant
 * This is the MAIN CLASS for the project
 * Adapt some codes from Steve's
 */
public class MyRestaurant extends Frame implements GLEventListener, ActionListener,
													ItemListener, MouseMotionListener {

	public final static int WIDTH=800;
	public final static int HEIGHT=800;
	private static final float NEAR_CLIP=0.1f;
	private static final float FAR_CLIP=100.0f;    
	private static final boolean CONTINUOUS_ANIMATION = false;

	private Point lastpoint;  // used with mouse routines
	private int width, height;

	private Checkbox checkAxes, checkObjects, checkLight0, 
					checkEyeLight, checkRobotView, checkCeilingLights;
	private Button startAnim, pauseAnim, resetScene;
	private boolean continuousAnimation = CONTINUOUS_ANIMATION;

	private Camera camera;
	private MyRestaurantScene scene;
	private GLCanvas canvas;

	public static void main(String[] args) {
		MyRestaurant gl = new MyRestaurant();
		gl.setVisible(true);
	}

	/**
	 * Constructor to instantiate MyRestaurant class	 
	 */
	public MyRestaurant() {
		setTitle("My Restaurant");  //title of canvas
		setSize(WIDTH, HEIGHT);

		GLProfile glp = GLProfile.getDefault();
		GLCapabilities caps = new GLCapabilities(glp);
		canvas = new GLCanvas(caps);
		add(canvas, "Center");
		canvas.addGLEventListener(this);
        
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		} );
	
		MenuBar menuBar = new MenuBar();
		setMenuBar(menuBar);
		Menu fileMenu = new Menu("File");
			MenuItem quitItem = new MenuItem("Quit");
			quitItem.addActionListener(this);
			fileMenu.add(quitItem);
		menuBar.add(fileMenu);

		Panel p = new Panel(new GridLayout(2,1));
		Panel p1 = new Panel(new GridLayout(6,1));
			checkAxes = addCheckbox(p1,"Axes On",this,true);  //axes on
			checkObjects = addCheckbox(p1,"Objects On",this,true);  //show objects
			checkLight0 = addCheckbox(p1,"Room Light On",this,true);  	//room light on
			checkCeilingLights = addCheckbox(p1,"Ceiling Spotlights On",this,true); //ceiling light on
			checkEyeLight = addCheckbox(p1,"Eye Lights On",this,true); 	//robot eyes lights on
			checkRobotView = addCheckbox(p1,"Robot View On",this,false);  	//set camera to robot view 
		p.add(p1);
		p1 = new Panel(new GridLayout(3,1));			
			startAnim = new Button("Start Animation");
			startAnim.setActionCommand("StartAnim");
			startAnim.addActionListener(this);
			p1.add(startAnim);
			pauseAnim = new Button("Pause Animation");
			pauseAnim.setActionCommand("PauseAnim");
			pauseAnim.addActionListener(this);
			p1.add(pauseAnim);
			resetScene = new Button("Reset Scene");
			resetScene.setActionCommand("ResetScene");
			resetScene.addActionListener(this);
			p1.add(resetScene);
		p.add(p1);
		add(p, "East");

		canvas.addMouseMotionListener(this); 

		FPSAnimator animator=new FPSAnimator(canvas, 30);
		animator.start();
	}
  
	private Checkbox addCheckbox(Panel p, String s, ItemListener a, boolean initState) {
		Checkbox c = new Checkbox(s, initState);
		c.addItemListener(a);
		p.add(c);
		return c;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equalsIgnoreCase("rotate")) {
			canvas.repaint();
		}
		else if(e.getActionCommand().equalsIgnoreCase("quit")) {
			System.exit(0);
		}
		else if (e.getActionCommand().equalsIgnoreCase("startanim")) {
			setContinuousAnimation(true);
		}
		else if (e.getActionCommand().equalsIgnoreCase("pauseanim")) {
			setContinuousAnimation(false);
		}
		else if (e.getActionCommand().equalsIgnoreCase("resetscene")) {
			reset();
		}
	}

	public void itemStateChanged(ItemEvent e) {
		Object source = e.getSource();
		if (source == checkAxes) {    //axes
			scene.getAxes().setSwitchedOn(checkAxes.getState());
		}
		else if (source == checkObjects) {
			scene.setObjectsDisplay(checkObjects.getState());
		}
		else if (source == checkLight0) {    //world light system
			scene.getLight().setSwitchedOn(checkLight0.getState());
			scene.setRoomLight(checkLight0.getState());
		}
		else if (source == checkEyeLight){    //robot eye spotlights
			scene.setEyeLight(checkEyeLight.getState());
			scene.getEyeLLight().setSwitchedOn(checkEyeLight.getState());
			scene.getEyeRLight().setSwitchedOn(checkEyeLight.getState());			
		}
		else if (source == checkRobotView){   //robot point of view
			scene.setRobotView(checkRobotView.getState());
		}
		else if (source == checkCeilingLights){    //ceiling spotlights
			scene.setCeilingLights(checkCeilingLights.getState());
			scene.getCeilingLight1().setSwitchedOn(checkCeilingLights.getState());
			scene.getCeilingLight2().setSwitchedOn(checkCeilingLights.getState());
		}
		canvas.repaint();
	}
  
	private void setContinuousAnimation(boolean b) {
		continuousAnimation = b;
	}

	/**
	 * Reset the scene and all checkboxes
	 */
	private void reset() {
		//reset axes
		checkAxes.setState(true);
		scene.getAxes().setSwitchedOn(true);
		
		//reset objectsOn
		checkObjects.setState(true);
		scene.setObjectsDisplay(true);
		
		//reset room light
		checkLight0.setState(true);
		scene.getLight().setSwitchedOn(true);
		scene.setRoomLight(true);
		
		//reset robot eye spotlights (L & R)
		checkEyeLight.setState(true);
		scene.setEyeLight(true);
		scene.getEyeLLight().setSwitchedOn(true);
		scene.getEyeRLight().setSwitchedOn(true);
		
		//reset ceiling spotlights
		checkCeilingLights.setState(true);
		scene.setCeilingLights(true);
		scene.getCeilingLight1().setSwitchedOn(true);
		scene.getCeilingLight2().setSwitchedOn(true);
		
		//reset to default viewing (not robot view)
		checkRobotView.setState(false);
		scene.setRobotView(false);
				
		setContinuousAnimation(CONTINUOUS_ANIMATION);
		scene.reset();
	}

	/*
	* METHODS DEFINED BY GLEventListener
	*/
	/* initialisation */
	public void init (GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //black
		gl.glEnable(GL2.GL_DEPTH_TEST); 
		gl.glEnable(GL2.GL_CULL_FACE);  
		gl.glCullFace(GL2.GL_BACK);    
                                    
		gl.glShadeModel(GL2.GL_SMOOTH); 	                                  
		gl.glPolygonMode(GL2.GL_FRONT_AND_BACK, GL2.GL_FILL);
	                                  
		gl.glEnable(GL2.GL_LIGHTING); 									
		gl.glEnable(GL2.GL_LIGHT0);     
		gl.glEnable(GL2.GL_NORMALIZE);
		
		double radius = 60.0;           
		double theta = Math.toRadians(-45); 
		double phi = Math.toRadians(40);
	
		camera = new Camera(theta, phi, radius);
		scene = new MyRestaurantScene(gl, camera);
	}
   
	/* Called to indicate the drawing surface has been moved and/or resized  */
	public void reshape (GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();

		this.width=width;
		this.height=height;
    
		scene.setCanvasSize(width,height);
    
		float fAspect=(float) width/height;
		float fovy=60.0f;

		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
    
		float top=(float) Math.tan(Math.toRadians(fovy*0.5))*NEAR_CLIP;
		float bottom=-top;
		float left=fAspect*bottom;
		float right=fAspect*top;
    
		gl.glFrustum(left, right, bottom, top, NEAR_CLIP, FAR_CLIP);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

	/* draw */
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();
		if (continuousAnimation) scene.update(gl);
		scene.render(gl);
	}

	public void dispose(GLAutoDrawable drawable) {
	}

	/**
	* mouse is used to control camera position
	* @param e  instance of MouseEvent
	*/    
	public void mouseDragged(MouseEvent e) {
		Point ms = e.getPoint();
    
		float dx=(float) (ms.x-lastpoint.x)/width;
		float dy=(float) (ms.y-lastpoint.y)/height;
    
		if (e.getModifiers()==MouseEvent.BUTTON1_MASK){
			camera.updateThetaPhi(-dx*2.0f, dy*2.0f);
		}else if (e.getModifiers()==MouseEvent.BUTTON3_MASK){
			camera.updateRadius(-dy);
		}
		lastpoint = ms;
	}

	/**
	* mouse is used to control camera position
	* @param e  instance of MouseEvent
	*/  
	public void mouseMoved(MouseEvent e) {   
		lastpoint = e.getPoint(); 
	}
}
