/**
 * I declare that this code is my own work
 * except last method borrowed from Steve's
 * Author: Phan Trung Hai Nguyen
 * Email: pnguyen1@sheffield.ac.uk
 */
 
import java.awt.*;
import java.awt.event.*;
import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.*;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.glu.GLUquadric;
import static java.lang.Math.*;

/**
 * A class representing a robot waiter
 * @author Phan Trung Hai Nguyen
 */ 
public class Robot {
	
	private GLU glu = new GLU();
	private GLUT glut = new GLUT();
	private GL2 gl;	
	
	private AnimationScene animationScene;
	private double bodyR;  //robot body's radius 
	private double headR;  //robot head's radius
	private boolean eyeLightOn;
	
	//3 items on tray
	private double item1, item2, item3;	
	
	/**
	 * Constructor to instantiate Robot class
	 * @param gl A GL2 object
	 * @param animationScene instance of animation control class
	 * @param bodyR robot body's radius
	 * @param headR robot head's radius
	 * @param eyeLightOn state of robot eye
	 */
	public Robot(GL2 gl, AnimationScene animationScene, double bodyR, double headR, boolean eyeLightOn){
		this.gl = gl;
		this.animationScene = animationScene;
		this.bodyR = bodyR;
		this.headR = headR;
		this.eyeLightOn = eyeLightOn;
	}
	
	/**
	 * Display the robot to the scene
	 */
	public void displayRobot(){
		gl.glPushMatrix();  
			double theta1 = animationScene.getParam(animationScene.ARM_R_PARAM);	
			double theta2 = animationScene.getParam(animationScene.ARM_L_PARAM);
			transformRobot();
			drawRobot(theta1, theta2); 
		gl.glPopMatrix();
	}
	
	/**
	 * turn on/off robot eyes spotlights
	 */
	public void setEyeLight(boolean b) {
		eyeLightOn = b;
	}	
	
	/**
	 * Set the camera to robot's view (i.e. advanced option)
	 */
	public void setRobotView(){
		
		//looking direction 
		double theta = toRadians(animationScene.getParam(animationScene.ROBOT_THETA_PARAM));
		double dx = headR*sin(theta);
		double dz = headR*cos(theta);
		double dy = 0;			

		//position of robot head in world coordinate
		double rx = animationScene.getParam(animationScene.ROBOT_X_PARAM);
        double rz = animationScene.getParam(animationScene.ROBOT_Z_PARAM);
		double ry = bodyR*2 + headR;	
		
		glu.gluLookAt(rx+dx,ry+dy,rz+dz,rx+2*dx,ry+dy,rz+2*dz,0,1,0);
	}
	
	/**
	 * Transform spotlights to robot eye's position
	 * @param theta the position of eye by rotating in y-axis
	 */
	public void transformSpotLightToRobotEye(double theta){
		theta = toRadians(theta);
		double dy1 = headR/3.0;
		double a = sqrt(headR*headR-dy1*dy1);
		double dx1 = a*sin(theta);
		double dz1 = a*cos(theta);	
		
		//swing head a theta angle
		double alpha = toRadians(animationScene.getParam(animationScene.SWING_HEAD_PARAM));		
		double dz = dz1;
		double dy = dy1*cos(alpha) + dx1*sin(alpha);
		double dx = dy1*sin(alpha) - dx1*cos(alpha);
		gl.glTranslated(dx,dy+2*bodyR+headR,dz);
	}
	
	/**
	 * Move robot around the room
	 */
	public void transformRobot(){
		//position of robot in world coordinate system
		double rx = animationScene.getParam(animationScene.ROBOT_X_PARAM);
        double rz = animationScene.getParam(animationScene.ROBOT_Z_PARAM);
		double theta = animationScene.getParam(animationScene.ROBOT_THETA_PARAM);		
		gl.glTranslated(rx,0,rz);
		gl.glRotated(theta,0,1,0);
		
		double alpha1 = animationScene.getParam(animationScene.LEANING_BACKWARD_PARAM); //leaning backward
        double alpha2 = animationScene.getParam(animationScene.LEANING_LEFT_RIGHT_PARAM); //leaning left-right
		gl.glRotated(alpha1,1,0,0); //leaning backward
		gl.glRotated(alpha2,0,0,1); //leaning left/right
	}
	
	/**
	 * Draw the robot (in hierarchical order)
	 * @param theta1 rotating left hand
	 * @param theta2 rotating right hand
	 */
	public void drawRobot(double theta1, double theta2){
		setMaterial(0.658f, 0.658f, 0.658f);	
		
		GLUquadric quadric = glu.gluNewQuadric();
		glu.gluQuadricDrawStyle(quadric, GLU.GLU_FILL);		
	
		gl.glPushMatrix();    			
			gl.glTranslated(0,bodyR,0);  //lift robot into x-z horizontal plane
			glut.glutSolidSphere(bodyR, 30, 30);  //robot body
			gl.glPushMatrix();
				drawButton(0.25, -35, bodyR);  //shirt button
				drawButton(0.25, -15, bodyR);
				drawButton(0.25, 5, bodyR);	 
				drawHead(quadric, headR, bodyR);  //head    
				drawShoulder(quadric, 1.7, -20, theta2, true);  //hand R
				drawShoulder(quadric, -1.7, 20, theta1, false); //hand L 
			gl.glPopMatrix();
		gl.glPopMatrix();  
	
		glu.gluDeleteQuadric(quadric);	
	}  
  
    /**
	 * Draw robot head
	 * @param quadric a GLUquadric object
	 * @param headR robot head's radius
	 * @param bodyR robot body's radius
	 */
	private void drawHead(GLUquadric quadric, double headR, double bodyR){
		gl.glPushMatrix();     
         	gl.glTranslated(0,headR+bodyR,0);
			
			//swing head a theta angle
			double theta = animationScene.getParam(animationScene.SWING_HEAD_PARAM);	
			gl.glRotated(theta,0,0,1);			
			glut.glutSolidSphere(headR, 20, 20); //robot head
			gl.glPushMatrix();
				drawNose(headR);			
				drawEye(0.15,headR,-10);  //Eye R
				drawEye(0.15,headR,10);   //eye L				
				drawHat(quadric);
			gl.glPopMatrix();
        gl.glPopMatrix();
	}
	
	/**
	 * Draw robot nose
	 * @param headR robot head's radius
	 */
	private void drawNose(double headR){
		setMaterial(1.0f, 0.62f, 0.0f);	
		gl.glPushMatrix(); 
			gl.glTranslated(0,0,headR);
			glut.glutSolidCone(0.3,1,20,20); //nose
		gl.glPopMatrix();
		setMaterial(0.0f, 0.0f, 0.0f);	
	}
     
	/**
	 * Draw robot shoulder
	 * @param quadric a GlUquadric object
	 * @param x 
	 * @param theta1 position of hand (rotating in z-axis)
	 * @param theta2 piosition of hand (rotating in y-axis)
	 * @param hasTray hand carries tray or not
	 */
	private void drawShoulder(GLUquadric quadric, double x, double theta1, 
										double theta2, boolean hasTray){
		setMaterial(0.412f, 0.412f, 0.412f);	//grey color
		gl.glPushMatrix();	
			gl.glTranslated(x,2.47,0);		
			glut.glutSolidSphere(0.45, 10, 10);
			drawUpperArm(quadric, theta1, theta2, hasTray);				
        gl.glPopMatrix(); 
		setMaterial(1.0f, 1.0f, 1.0f); 		
	}	
	
	/**
	 * Draw the hat of the robot
	 * @param quadric a GLUquadric object
	 */
	private void drawHat(GLUquadric quadric){
		setMaterial(0.0f, 0.0f, 0.0f);	
		gl.glPushMatrix(); //hat              	
			gl.glTranslated(0,1,0);
            gl.glPushMatrix(); 					
				gl.glScaled(1,0.05,1);
				gl.glTranslated(0,2,0);                    					
				glut.glutSolidSphere(2.0,20,20);
			gl.glPopMatrix();  
            gl.glPushMatrix();	
				gl.glRotated(-90,1,0,0);
				glu.gluCylinder(quadric, 1.118f, 1.5f, 2, 20, 20);	
				gl.glTranslated(0,0,2);
                glu.gluDisk(quadric,0.0,1.5,20,20);	//top lid			
            gl.glPopMatrix(); 				
        gl.glPopMatrix();  
	}
   
    
	/**
	 * Draw upper arm for robot
	 * @param theta1 
	 * @param theta2
	 * @param hasTray hand carries tray or not
	 */
	private void drawUpperArm(GLUquadric quadric, double theta1, double theta2, boolean hasTray){
		gl.glPushMatrix();
			gl.glRotated(20,0,0,1);
			gl.glRotated(theta1,0,0,1);
			gl.glRotated(theta2,0,1,0);
			glu.gluCylinder(quadric,0.25f,0.25f,2.5f,20,20);
			gl.glPushMatrix();
				gl.glTranslated(0,0,2.5);		
				glut.glutSolidSphere(0.3, 10, 10);
				drawForeArm(quadric, hasTray);
			gl.glPopMatrix();
        gl.glPopMatrix();
	}
  
	/**
	 * Draw fore arm
	 * @param quadric an GLUquadric object
	 * @param hasTray hand carries tray or not
	 */
	private void drawForeArm(GLUquadric quadric, boolean hasTray){
		gl.glPushMatrix(); 						
			gl.glRotated(-60,1,0,0);
			glu.gluCylinder(quadric,0.2f,0.2f,2f,20,20);					
			gl.glPushMatrix();
				gl.glTranslated(0,0,2);		
				glut.glutSolidSphere(0.25, 10, 10);
				if (!hasTray){
					double theta1 = animationScene.getParam(animationScene.CLAW1_PARAM);	
					double theta2 = animationScene.getParam(animationScene.CLAW2_PARAM);
					drawFinger(theta1);
					drawFinger(theta2);
				} else {
					drawFinger(55);
					drawFinger(-30);
				}				
				if (hasTray) drawTray();
			gl.glPopMatrix();
		gl.glPopMatrix();
	}   
	
	/**
	 * Draw fingers (claws) for robot
	 * @param theta position of claw 
	 */
	private void drawFinger(double theta){
		gl.glPushMatrix(); //finger 02
			gl.glRotated(theta,1,0,0);
			gl.glScaled(0.3,0.3,1);
            gl.glTranslated(0,0,0.5);								
			glut.glutSolidSphere(0.5, 10, 10);
		gl.glPopMatrix();
	}
  
    /**
	 * Draw shirt button for robot
	 * @param r radius of button
	 * @param theta position of button in shirt
	 * @param z position of button in z-axis
	 */
    private void drawButton(double r, double theta, double z){
		setMaterial(1.0f, 0.0f, 0.0f); //red color
	    gl.glPushMatrix();
			gl.glRotated(theta,1,0,0); 
			gl.glTranslated(0,0,z);
			glut.glutSolidSphere(r,20,20);
		gl.glPopMatrix();
		setMaterial(1.0f, 1.0f, 1.0f);	

    }
	
	/**
	 * Draw robot eyes
	 * @param r radius of robot eye
	 * @param headR robot head's radius
	 * @param theta position of robot head
	 */
	private void drawEye(double r, double headR, double theta){		
		if (eyeLightOn) setEmission(0.8f,0.8f,0.8f);
		else setEmission(0.0f,0.0f,0.0f);	
		
		theta = toRadians(theta);
		double dy = headR/3.0;
		double a = sqrt(headR*headR-dy*dy);
		double dx = a*sin(theta);
		double dz = a*cos(theta);

		gl.glPushMatrix(); 			
			gl.glTranslated(dx,dy,dz);
			glut.glutSolidSphere(r,10,10);
		gl.glPopMatrix();
		
		float[] default_emission = {0.0f, 0.0f, 0.0f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, default_emission, 0);
	} 	
	
	/**
	 * Draw a tray with some items on it
	 */
	private void drawTray(){
		setMaterial(0.66f, 0.35f, 0.0f);
		double item1 = animationScene.getParam(animationScene.ITEM1_PARAM);
		double item2 = animationScene.getParam(animationScene.ITEM2_PARAM);
		double item3 = animationScene.getParam(animationScene.ITEM3_PARAM);
		
		gl.glPushMatrix();
			gl.glTranslated(0,0.0,0.2);
			gl.glRotated(55,1,0,0);
			gl.glPushMatrix();
				gl.glScaled(1,0.05,1);
				gl.glTranslated(0,1.5,1.5);
				glut.glutSolidCube(3f); //tray
			gl.glPopMatrix();
			gl.glPushMatrix();
				if (item1 > 0.0) drawItemOnTray(1.0f,0.0f,0.0f,0.8,0.65,1.0,1.0f);				
				if (item2 > 0.0) drawItemOnTray(0.76f,0.79f,0.0f,-1.0,0.40,1.5,0.5f);					
				if (item3 > 0.0) drawItemOnTray(0.75f,0.18f,0.71f,0.0,0.40,2.5,0.5f);						
			gl.glPopMatrix();			
		gl.glPopMatrix();
		setMaterial(0.658f, 0.658f, 0.658f);	
	}
	
	/**
	 * Draw an item positioned on tray
	 * @param mat1 material property
	 * @param mat2 material property
	 * @param mat3 material property
	 * @param x x-coordinate of item
	 * @param y y-coordinate of item
	 * @param z z-coordinate of item
	 * @param size size of item
	 */
	private void drawItemOnTray(float mat1, float mat2, float mat3, 
									double x, double y, double z, float size){
		gl.glPushMatrix();
			setMaterial(mat1,mat2,mat3);	
			gl.glTranslated(x,y,z);
			glut.glutSolidCube(size); 
		gl.glPopMatrix();
	}	
	
	/**
	 * Set emission property for object
	 * @param a
	 * @param b
	 * @param c
	 */
	private void setEmission(float a, float b, float c){
		float[] emission = {a, b, c, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, emission, 0);
	}	
	
	/**
	 * set material properties for object
	 * Borrowed from Steve's
	 * @param r red coponent
	 * @param g glue component
	 * @param b blue component
	 */
	private void setMaterial(float r, float g, float b) {
		float[] matAmbientDiffuse = {r,g,b, 1.0f};
		float[] matSpecular = {0.5f,0.5f,0.5f, 1.0f};
		float[] matShininess = {16.0f};
		float[] matEmission = {0.0f, 0.0f, 0.0f, 1.0f};
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT_AND_DIFFUSE, matAmbientDiffuse, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, matSpecular, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SHININESS, matShininess, 0);
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_EMISSION, matEmission, 0);
	}
}