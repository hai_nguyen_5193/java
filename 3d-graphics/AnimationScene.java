/**
 * A class representing animation control of the room
 * Adapt some codes from Steve's
 */
public class AnimationScene {
	
	public static final int ROBOT_X_PARAM = 0;
	public static final int ROBOT_Z_PARAM = 1;  
	public static final int ROBOT_THETA_PARAM = 2;  	
	public static final int ARM_R_PARAM = 3;
	public static final int ARM_L_PARAM = 4;
	public static final int CLAW1_PARAM = 5;
	public static final int CLAW2_PARAM = 6;
	public static final int SWING_HEAD_PARAM = 7;
	public static final int CEILING_LIGHT_PARAM = 8;
	public static final int CHRISTMAS_TREE_LIGHT_PARAM = 9;
	public static final int LEANING_BACKWARD_PARAM = 10;
	public static final int LEANING_LEFT_RIGHT_PARAM = 11;
	public static final int ITEM1_PARAM = 12;
	public static final int ITEM2_PARAM = 13;
	public static final int ITEM3_PARAM = 14;	
  
	public static final int MAX_PARAMS = 15;
	private Anim[] param;
	private int numParams;
	private double globalStartTime, localTime, repeatTime, savedLocalTime;     
  
	/**
	 * Constructor to instantiate AnimationScene class	 
	 * @author Phan Trung Hai Nguyen
	 */
	public AnimationScene(){
		param = new Anim[MAX_PARAMS];
		
		//robot x-coordinate
		param[ROBOT_X_PARAM] = create(0.0, 15.0, true, true,  
									new double[]{0.0,27.0, 0.05,21.0, 0.1,18.0, 0.15,16.0, 0.2,16.0, 0.24,16.0,
												0.267, 22.0, 0.294,25.0, 0.321,26.0, 0.348,24.0, 0.375,20.0, 0.402,14.0,
												0.432,8.0, 0.492,-3.0, 0.52,-9.0, 0.56,-9.0, 0.6,-10.0, 0.64,-12.0,
												0.68,-15.0, 0.72,-16.0, 0.76,-16.0, 0.80,-8.0, 0.856,0.0, 0.904,8.0, 
												0.952,18.0, 1.0,27.0});
	
		//robot z-coordinate
		param[ROBOT_Z_PARAM] = create(0.0, 15.0, true, true, 
									new double[]{0.0,-20.0, 0.05,-15.0, 0.1,-8.0, 0.15,0.0, 0.2,9.0, 0.24,9.0,
												0.267,9.0, 0.294,13.0, 0.321,18.0, 0.348,23.0, 0.375,25.0, 0.402,25.0,
												0.432,23.0, 0.492,17.0, 0.52,16.0, 0.56,16.0, 0.6,10.0, 0.64,5.0,
												0.68,-2.0, 0.72,-9.0, 0.76,-9.0, 0.80,-10.0, 0.856,-12.0, 0.904,-16.0, 
												0.952,-20.0, 1.0,-20.0});
		//direction of robot
		param[ROBOT_THETA_PARAM] = create(0.0, 15.0, true, true, 
									new double[]{0.0,-90.0, 0.05,-35.0, 0.1,-20.0, 0.15,-5.0, 0.2,0.0, 0.24,0.0,
												0.267,70.0, 0.294,20.0, 0.321,-5.0, 0.348,-40.0, 0.375,-70.0, 0.402,-100.0,
												0.432,-125.0, 0.492,-115.0, 0.52,-90.0, 0.56,-90.0, 0.6,-160.0, 0.64,-155.0,
												0.68,-160.0, 0.72,-180.0, 0.76,-180.0, 0.80,-260.0, 0.856,-248.0, 0.904,-235.0, 
												0.952,-260.0, 1.0,-270.0});
		//animation for right hand of robot
		param[ARM_R_PARAM] = create(0.0, 15.0, true, true, 
									new double[]{0.0,-90.0, 0.1,-120, 0.2,-90, 0.22,30.0, 0.24,-20.0, 
												0.402,-20.0, 0.432,-90.0, 0.492,-120.0, 0.53,-90.0, 0.545,20.0, 
												0.56,-90, 0.62,-80.0, 0.67,-120.0, 0.72,-90.0, 0.74,20.0, 0.76,-90.0, 
												0.64,-90, 0.68,-120, 0.72,-90.0, 0.74,30.0, 0.76,-90.0, 0.856,-120.0, 
												0.952,-70.0, 1.0,-90.0});
		//animation for left hand of robot
		param[ARM_L_PARAM] = create(0.0, 15.0, true, true, 
									new double[]{0.0,90.0, 0.2,90.0, 0.22,-30.0, 0.24,90.0, 0.267,-30.0,
												0.402,-30.0, 0.432,90.0, 0.53,90.0, 0.545,-30.0, 0.6,-30.0, 0.64,90.0,
												0.72,90.0, 0.74,-30.0, 0.80,-30.0, 0.856,90.0, 1.0,90.0});
		
		//animation for right hand claw 1
		param[CLAW1_PARAM] = create(0.0, 15.0, true, true, 
									new double[]{0.0,50.0, 0.1,0.0, 0.2,50.0, 0.22,0.0, 0.26,50.0,
												0.35,0.0, 0.47,50.0, 0.49,0.0, 0.51,50.0, 
									            0.53,50.0, 0.545,0.0, 0.72,50.0, 0.74,0.0, 0.76,50.0,
												0.9,0.0, 1.0,50.0});
											 
		//animation for right hand claw 2
		param[CLAW2_PARAM] = create(0.0, 15.0, true, true, 
									new double[]{0.0,-30.0, 0.1,0.0, 0.22,-30.0, 0.24,0.0, 0.26,-30.0,
												0.35,0.0, 0.47,-30.0, 0.49,0.0, 0.51,-30.0, 
									            0.6,-30.0, 0.7,0.0, 0.72,-30.0, 0.74,0.0, 0.76,-30.0,
												0.9,0.0, 1.0,-30.0});
											
		//swing robot head
		param[SWING_HEAD_PARAM] = create(0.0, 15.0, true, true, 
										new double[]{0.0,-10.0, 0.1,10.0, 0.22,-10.0, 0.267,10.0, 
													0.432,10.0, 0.50,0.0, 0.56,-10.0, 0.64,-10.0, 
													0.72,0.0, 0.76,-10.0, 0.856,-10.0, 0.9,10.0, 1.0,0.0});
		
		//rotating celling spotlights
		param[CEILING_LIGHT_PARAM] = create(0.0, 15.0, true, true, 
										new double[]{0.0,0.0, 0.1,-30.0, 0.2,0.0, 0.3,30.0, 0.4,0.0, 
													0.5,-30.0, 0.6,0.0, 0.7,30.0, 0.8,0.0, 0.9,-30.0, 1.0,0.0});
		
		//set emission for christmas tree
		param[CHRISTMAS_TREE_LIGHT_PARAM] = create(0.0, 15.0, true, true, 
												new double[]{0.0,0.0, 0.25,0.5, 0.5,0.0, 0.75,0.5, 1.0,0.0});
		
		
		//leaning backward
		param[LEANING_BACKWARD_PARAM] = create(0.0, 15.0, true, true, 
											new double[]{0.0,0.0, 0.15,0.0, 0.2,-10.0, 0.205,0.0, 0.492,0.0,
														0.52,-10.0, 0.53,0.0, 0.68,0.0, 0.72,-10.0, 
														0.725,0.0, 0.952,0.0, 0.98,-10.0, 1.0,0.0});
		
		//leaning left/right
		param[LEANING_LEFT_RIGHT_PARAM] = create(0.0, 15.0, true, true, 
											new double[]{0.0,0.0, 0.05,10.0, 0.1,0.0, 0.24,0.0, 0.267,-10.0,
														0.375,-10.0, 0.402,0.0, 0.56,0.0, 0.60,10.0, 
														0.64,0.0, 0.76,0.0, 0.80,10.0, 0.856,0.0, 1.0,0.0});
		//item 1 (on tray)
		param[ITEM1_PARAM] = create(0.0, 15.0, true, true, 
											new double[]{0.0,1.0, 0.22,0.0, 1.0,0.0});
											
		//item 2 (on tray)
		param[ITEM2_PARAM] = create(0.0, 15.0, true, true, 
											new double[]{0.0,1.0, 0.545,0.0, 1.0,0.0});
											
		//item 3 (on tray)
		param[ITEM3_PARAM] = create(0.0, 15.0, true, true, 
											new double[]{0.0,1.0, 0.74,0.0, 1.0,0.0});
														
		numParams = ITEM3_PARAM + 1;
		localTime = 0;
		savedLocalTime = 0;
		repeatTime = 15;
		globalStartTime = getSeconds();
	}
  
	public Anim create(double start, double duration, boolean pre, boolean post, double[] data) {
		KeyInfo[] k = new KeyInfo[data.length/2];
		for (int i=0; i<data.length/2; ++i) {
			k[i] = new KeyInfo(data[i*2], data[i*2+1]);
		}    
		return new Anim(start, duration, pre, post, k);
	}
  
	public void startAnimation() {
		globalStartTime = getSeconds() - savedLocalTime;
	}
  
	public void pauseAnimation() {
		savedLocalTime = getSeconds() - globalStartTime;
	}
  
	public void reset() {
		globalStartTime = getSeconds();
		savedLocalTime = 0;
		for (int i=0; i<numParams; ++i) {
			param[i].reset();
		}
	}
  
	private double getSeconds() {
		return System.currentTimeMillis()/1000.0;
	}
  
	public void update() {
		localTime = getSeconds() - globalStartTime;
		if (localTime > repeatTime) {
			globalStartTime = getSeconds();
			localTime = 0;
			savedLocalTime = 0;
		}  
		for (int i=0; i<numParams; ++i) {
			param[i].update(localTime);
		}
	}

	public double getParam(int i) {
		if (i<0 || i>=numParams) {
			System.out.println("EEError: parameter out of range");
			return 0;
		}else {
			return param[i].getCurrValue();
		}
	}
   
	public String toString() {
		String s = "Anim manager: ";
		return s;
	}  
}