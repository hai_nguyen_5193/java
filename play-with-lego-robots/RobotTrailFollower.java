import icommand.nxt.*;
import icommand.navigation.Pilot;
import icommand.nxt.comm.NXTCommand;
import java.util.*;

/**
* A class to represent a Robot
*@author Ben Couston & Phan Trung Hai Nguyen
*/
public class RobotTrailFollower {
	
	//Constants
	 static final int TURNING_SPEED = 200;
	 static final int WALKING_SPEED = 110;
	 static final int WALKING_TIME = 2000;
	 static final int TURN_TIME = 1725;
	 static final int AVERAGE = 40;
	 static final double EQUAL = 0.5;
	 static final double MAX = 1.40;
	 
	
	 //Constructor
	 /**
	 *Constructor creates a new object of the class RobotTrailFollower
	 */
	 public RobotTrailFollower(){}
	 

	 //Instance Methods
	 /**
	 *Method turns the Robot 90 degrees to the right
	 */
	 private void turnRight() throws InterruptedException {
	 	Motor.B.stop();
	 	Motor.C.stop();
	        Motor.B.setSpeed(TURNING_SPEED);
		Motor.B.forward();
		Thread.sleep(TURN_TIME + 65);
		Motor.B.stop();	
                Motor.B.setSpeed(WALKING_SPEED);
		Motor.C.setSpeed(WALKING_SPEED);
		Motor.B.forward();
		Motor.C.forward();		
	}

	
	/**
	*Method turns the Robot 90 degrees to the left
	*/
	private void turnLeft() throws InterruptedException {
		Motor.B.stop();
		Motor.C.stop();
	        Motor.C.setSpeed(TURNING_SPEED);
	 	Motor.C.forward();
		Thread.sleep(TURN_TIME);
		Motor.C.stop();	
                Motor.B.setSpeed(WALKING_SPEED);
		Motor.C.setSpeed(WALKING_SPEED);
		Motor.B.forward();
		Motor.C.forward();		
	}
	
	
	/**
	*Method causes the Robot to become stationary
	*/
	private void stop(){
	        tunez();
		Motor.B.stop();
		Motor.C.stop();
	}
	
	
	/**
	*Method causes the Robot to move in a straight line
   	*/
   	private void straight(){
                Motor.C.setSpeed(WALKING_SPEED);
		Motor.B.setSpeed(WALKING_SPEED);
		Motor.B.forward();
		Motor.C.forward();
        }
        
        //Static method
        /**
	*Method causes the Robot to make a beep sound
   	*/
        private static void tunez() {
        	Sound.playTone(1000,200); 
    	}
    	
    
    	/**
    	*Main body
    	*@param args String[]
    	*/
    	public static void main(String[] args) throws InterruptedException {
                
		NXTCommand.open();
		NXTCommand.setVerify(true);
				
		//Object called "robot" of class "RobotTrailFollower"
                RobotTrailFollower robot = new RobotTrailFollower();	
		robot.straight();
				
		//Object called "lightValue" of class "LightSensor"
		LightSensor lightValue = new LightSensor(SensorPort.S1);
		int light = lightValue.getLightPercent();
		
		//Object called "sc" of class "Pilot"
		Pilot sc = new Pilot(2.1f,4.4f, Motor.C, Motor.B);
		
		//Array to store the width of two bars
		double[] width = new double[2];
		
		//Variable to count the bars
		int count = -1;
		
		//Variable to stop while loop when reading the stop instruction
		boolean stop = false;
		
		
		while (!stop){
			
		    /**When the LightPercent becomes lower than AVERAGE, 
		   adding one to count and reseting the TachoCount. 
		   When the LightPercent larger than AVERAGE, 
		   the width of the first bar will be calculated 
		   */
		   if (light < AVERAGE){
			count++;
			sc.resetTachoCount();
			do {
		             light = lightValue.getLightPercent();
		             if (light >= AVERAGE) {
		                 width[count] = sc.getTravelDistance();
			     }
		        }while (light < AVERAGE);
		    }	
		    
                    light = lightValue.getLightPercent();
                    
                    /**When count equals 1, two bar widths have been read,
                    comparing two widths to interpret the instruction.
                    */                    
		    if (count == 1){
		    	    
		    	//Two bars are equal
			if (Math.abs(width[0]-width[1]) <= EQUAL){
			     
		             //Test thick or thin
			     if (width[0] >= MAX){
                                   robot.stop();
			 	   stop = true;
			     } else {
			        robot.straight();
			     }
			
			//Two bars are different
			} else {
				
			     //Test thick before or thin before
		             if (width[0] > width[1]) {
				  robot.turnRight();
			     } else {
			          robot.turnLeft();
			     }
		        }
		        
		        //Reset the count to continue
			count = -1;
		    }
		}
		NXTCommand.close();
  	 }

}