

import java.util.ArrayList;

/**
 * Record all actions during the game (cmd, time)
 * @author Phan Trung Hai Nguyen
 *
 */
public class History 
{
	
	//an ArrayList to store all cmd entered by the player
	public static ArrayList<History> hlist = new ArrayList<History>();
	
	private String cmd;
	private int time;
	
	/**
	 * constructor
	 */
	public History(String cmd, int time)
	{
		this.cmd = cmd;
		this.time = time;
	}
	
	public String getCmd()
	{
		return cmd;
	}
	
	public int getTime()
	{
		return time;
	}
	
}
