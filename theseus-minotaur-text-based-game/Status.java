import sheffield.*;

/**
 * represent the status of the game
 * @author Phan Trung Hai Nguyen
 *
 */
public class Status 
{
	private EasyWriter writer;
	
	/**
	 * Constructor
	 */
	public Status()
	{
		writer = new EasyWriter();
	}

	public void show(Player player, String cmd)
	{
		writer.println("Time elapsed: "+ Game.time);
		player.displayObject();
		History.hlist.add(new History(cmd, Game.time));
	}
}
