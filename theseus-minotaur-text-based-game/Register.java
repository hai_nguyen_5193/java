import sheffield.*;

import java.util.*;

/**
 * model the functions of administrator
 * @author Phan Trung Hai Nguyen
 *
 */
public class Register
{	
	//ArrayList consists of all player
	public static ArrayList<Player> playerList;
	
	EasyReader reader;
	EasyWriter writer;
	public static int index;;
	
	/**
	 * Constructor
	 */
	public Register()
	{
		writer = new EasyWriter();
		reader = new EasyReader();
		playerList = new ArrayList<Player>();
	}
	
	//check whether particular player registered or not
	//also find the index of that player
	public static boolean checkPlayer(String name)
	{
		index = -1;
		for (Player e: playerList)
		{
			index++;
			if (e.getName().equals(name)) 
				return true;
		}
		return false;
	}
    
	//3 methods
	public void registerPlayer(String name, String password)
	{
		playerList.add(new Player(name, password));
	}
	
	
	public void detectCommand(String cmd)
	{
			
		if (cmd.contains("register"))
		{
			String part2 = cmd.substring(cmd.indexOf(" ")+1);
			boolean check = checkPlayer(part2);
			if (check)
			{
				writer.println("Already registered");
			} 
			else 
			{
				//enter password for this new player
				System.out.print("Enter password \n> ");
				String pass = reader.readString();
				
				//add this player to the playerlist
				playerList.add(new Player(part2, pass));
				writer.println("Registered successfully");
			}
		} 
		else if (cmd.contains("edit"))
		{
			String part2 = cmd.substring(cmd.indexOf(" ")+1);
			if (checkPlayer(part2))
			{
				String pass = reader.readString("Enter new password \n>");
				playerList.get(index).setPassword(pass);		
				writer.println("Password changed successfully");
			} 
			else 
			{
				writer.println("Not yet registered!");
			}
		} 
		else if (cmd.contains("remove"))
		{
			String part2 = cmd.substring(cmd.indexOf(" ")+1);
			if (checkPlayer(part2))
			{
				playerList.remove(index);
				writer.println("Removed successfully");
			} 
			else 
			{
				writer.println("Not in system");
			}
		}
		else 
		{
			writer.println("Unrecognized command "+cmd);
		}
		
		
	}
}
