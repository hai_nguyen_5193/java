
import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Assert;
import org.junit.Test;

import sheffield.EasyReader;
import sheffield.EasyWriter;

import game.Game;

public class TestGame {

	protected Game game;
	public static final String newline = System.getProperty("line.separator");

	/** Tests that the game correctly responds to the supplied user name. */
	@Test
	public void testRun1()
	{
		String text = "TestUser";
		game = new Game(text);
		game.run();
		Assert.assertEquals("What is your name?Hello, TestUser"+newline+"Game terminated"+newline,game.getTestOutput());
	}

	/** Tests how the game responds to an end-of-data when asking for a user name. */
	@Test
	public void testRun2()
	{
		String text = "";
		game = new Game(text);
		game.run();
		Assert.assertEquals("What is your name?Hello, unknown"+newline+"Game terminated"+newline,game.getTestOutput());
	}
	
	/** Tests the response to an unrecognised command and a subsequent quit. */
	@Test
	public void testRun3()
	{
		String text = "Fred\nsome command";
		game = new Game(text);
		game.run();
		Assert.assertEquals("What is your name?Hello, Fred"+newline+"received unrecognised command: some command"+newline+"Game terminated"+newline,game.getTestOutput());
	}

	/** Tests the "name" command. */
	@Test
	public void testRun4()
	{
		String text = "Fred\nname\nquit";
		game = new Game(text);
		game.run();
		Assert.assertEquals("What is your name?Hello, Fred"+newline+"user name is Fred"+newline+"Game terminated"+newline,game.getTestOutput());
	}
	
	public static final double accuracy = 1e-12;
	
	/** Tests that numbers can be successully read in. */
	@Test
	public void testReader1()
	{
		StringWriter wr = new StringWriter();
		EasyReader reader=new EasyReader(new StringReader("34.2\n4.9"),wr);
		Assert.assertEquals(34.2,reader.readDouble(),accuracy);
		Assert.assertEquals(4.9,reader.readDouble(),accuracy);
		Assert.assertTrue(wr.toString().isEmpty());// check that no output is produced by the reader
	}
	
	/** Tests the reader with a different line ending. */
	@Test
	public void testReader2()
	{
		StringWriter wr = new StringWriter();
		EasyReader reader=new EasyReader(new StringReader("34.2\r\n4.9"),wr);
		Assert.assertEquals(34.2,reader.readDouble(),accuracy);
		Assert.assertEquals(4.9,reader.readDouble(),accuracy);
		Assert.assertTrue(wr.toString().isEmpty());// check that no output is produced by the reader
	}
	
	/** Tests that numbers and boolean can be read in. Note that I'm using not only \n but also \r as separators. */
	@Test
	public void testReader3()
	{
		StringWriter wr = new StringWriter();
		EasyReader reader=new EasyReader(new StringReader("34.2\n4.9\rtrue\r2"),wr);
		Assert.assertEquals(34.2,reader.readDouble(),accuracy);
		Assert.assertEquals(4.9,reader.readDouble(),accuracy);
		Assert.assertEquals(true,reader.readBoolean());
		Assert.assertEquals(2,reader.readInt());
		Assert.assertTrue(wr.toString().isEmpty());// check that no output is produced by the reader
	}
		
	/** Test that numbers and booleans are output correctly. */
	@Test
	public void testWriter()
	{
		StringWriter wr = new StringWriter();
		EasyWriter writer = new EasyWriter(wr);
		writer.print(23.4);writer.print(true);writer.println();writer.print('j');writer.println();writer.print(33);writer.print(66L);
		writer.print('a');
		Assert.assertEquals("23.4true"+newline+"j"+newline+"3366a",wr.toString());
		writer.println();// this makes sure the buffered data inside the writer is echoed on screen.
		writer.close();
	}
	
	/** Tests that attempting to read a number as if it were a boolean fails (exception is thrown). */
	@Test(expected=RuntimeException.class)
	public void testReaderFails()
	{
		StringWriter wr = new StringWriter();
		new EasyReader(new StringReader("34.2"),wr).readBoolean();
	}
}
