
import java.util.ArrayList;
import sheffield.*;

/**
 * model the user of the game
 * @author Phan Trung Hai Nguyen
 *
 */
public class Player 
{	
	protected EasyWriter writer;
	protected String name;
	protected String password;
	protected int strength;
	protected Maze maze;
	protected int cX, cY;
	
	//Array to store the objects
	protected ArrayList<String> objects;
	
	/**
	 * Constructor
	 * @param name name of user
	 * @param password password of user
	 */
	public Player(String name, String password)
	{
		writer = new EasyWriter();
		this.name = name;
		this.password = password;
		objects = new ArrayList<String>();
		strength = 0;
	}
	
	//display all objects carried by the player
	public void displayObject()
	{
		for (int i=0; i < objects.size(); i++)
		{
			writer.println(objects.get(i));
		}
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getPassowrd()
	{
		return password;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public void setMaze(Maze maze)
	{
		this.maze = maze;
	}
	
	public void setCurrentX(int x)
	{
		cX = x;
	}
	
	public void setCurrentY(int d)
	{
		cY = d;
	}
	
	public String getPosition()
	{
		return ""+cY+cX;
	}

}
