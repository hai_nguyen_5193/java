

/**
 * represent the moving of player
 * @author Phan Trung Hai Nguyen
 *
 */
public class Move 
{
		
	/**
	 * Constructor
	 */
	public Move()
	{}
	
	//moving north
	public void moveN(Player player)
	{
		String position = ""+player.cY + player.cX +(player.cY-1) + (player.cX);
		if ((player.cY == 0)
				|| (player.maze.checkWall(position)))
		{
			System.out.println("Hitting wall");
		}
		else 
		{
			player.cY -= 1;
			
			//time unit consumed equals no. of objects carried
			Game.time += player.objects.size();
		}
	}
	
	//moving south
	public void moveS(Player player)
	{
		String position = ""+player.cY + player.cX +(player.cY+1) + (player.cX);
		if ((player.cY == (player.maze.getLength()-1))
				|| (player.maze.checkWall(position)))
		{
			System.out.println("Hitting wall");
		} 
		else 
		{
			player.cY += 1;
			
			//time unit consumed equals no. of objects carried
			Game.time += player.objects.size();
		}
	}
	
	//moving west
		public void moveW(Player player)
		{
			String position = ""+player.cY + player.cX +(player.cY) + (player.cX-1);
			if ((player.cX == 0)
					|| (player.maze.checkWall(position)))
			{
				System.out.println("Hitting wall");
			} else {
				player.cX -= 1;
				
				//time unit consumed equals no. of objects carried
				Game.time += player.objects.size();
			}
		}
		
		//moving east
		public void moveE(Player player)
		{
			String position = ""+player.cY + player.cX +(player.cY) + (player.cX +1);
			if ((player.cX == (player.maze.getLength()-1))
					|| (player.maze.checkWall(position)))
			{
				System.out.println("Hitting wall");
			} 
			else 
			{
				player.cX += 1;
				
				//time unit consumed equals no. of objects carried
				Game.time += player.objects.size();
			}
		}
	
}
