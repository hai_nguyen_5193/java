

/**
 * model the fighting action
 * @author Phan Trung Hai Nguyen
 *
 */
public class Fighting 
{
	
	/**
	 * Constructor
	 */
	public Fighting()
	{};
	
	public String fightResult(Player player)
	{
		int strength = player.strength;
		if (strength >= 30) 
		{
			return "win";
		}
		else if (strength >=15) 
		{
			int r = (int) (10*Math.random())%2;
			if (r == 1) 
			{
				return "win";
			}
			else
			{
				return "lose";
			}
		} 
		else 
		{
			return "lose";
		}
	}

}
