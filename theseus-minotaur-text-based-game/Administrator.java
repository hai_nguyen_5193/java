
/**
 * This model represents the Administrator
 * @author Phan Trung Hai Nguyen
 *
 */
public class Administrator 
{
	private String password = "admin12345";
	
	/**
	 * Constructor
	 */
	public Administrator()
	{}
	
	//ask the admin to insert password, return true if correct password
	public boolean checkPassword(String pass)
	{
		return pass.equals(this.password) ;
	}

}
