

/**
 * Model represent picking/drop actions of the user
 * @author Phan Trung Hai Nguyen
 *
 */
public class Picking 
{

	/**
	 * constructor
	 */
	public Picking()
	{};
	
	/**
	 * method to check objects full or not
	 */
	public boolean checkFull(Player player)
	{
		return (player.objects.size()==3);
	}
	
	/**
	 * check whether the player carries a particular object or not
	 * @param player the current player
	 * @param object object needs to be checked
	 * @return TRUE if player carries this object, and FALSE otherwise
	 */
	public boolean checkObject(Player player, String object)
	{
		for (String e: player.objects)
		{
			if (e.equals(object)) 
				return true;
		}
		return false;
	}
	
	/**
	 * pick up object if available
	 * object increase strength by 3
	 * 
	 */
	public void pickUp(Player player, String content)
	{
		if (!checkFull(player))
		{
			player.objects.add(content);
			player.strength +=5;
			String position = player.getPosition();
			
			switch(content)
			{
		        case "sword": 
		    	    player.maze.sword.remove(position);
		    	    break;
		        case "shield": 
		    	    player.maze.shield.remove(position);
		    	    break;
		        case "knife": 
		        	player.maze.knife.remove(position);
		    	    break;
		    }
						
		}
		else 
		{
			System.out.println("Objects full");
		}
	}
	
	/**
	 * dropping object into the current cell
	 * 
	 */
	public void drop(Player player, String object)
	{
		if (checkObject(player, object))
		{
		    int index = -1;
		    for (int i =0; i<player.objects.size(); i++)
		    {
			    if (player.objects.get(i).equals(object))
			    {
				    index = i;
			    }
		    }
		    player.objects.remove(index);
		    player.strength -= 5;
		    String position = player.getPosition();
		    switch(object)
		    {
		        case "sword": 
		        	player.maze.sword.add(position);
		    	    break;
		        case "shield":
		        	player.maze.shield.add(position);
		        	break;
		        case "knife": 
		        	player.maze.knife.add(position);
		        	break;
		    }
		}
		else 
		{
			System.out.println("object not carried");
		}
		
	}
}
