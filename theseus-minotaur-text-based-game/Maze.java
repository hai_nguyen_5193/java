

import java.util.ArrayList;

/**
 * model the maze
 * @author Phan Trung Hai Nguyen
 *
 */
public class Maze 
{
	//the maze represented as an 2D array
	public static int length;
	protected int[][] maze;
	protected String[] wall;
	protected ArrayList<String> sword;
	protected ArrayList<String> knife;
	protected ArrayList<String> shield;
	protected String minotaur;
	
	
	public Maze(){}
	
	/**
	 * constructor
	 * @param r rows
	 * @param c columns
	 */
	public Maze(int num)
	{
		length = num;
		maze = new int[num][num];
			
		if (num == 3)
		{
			wall = new String[]{"2212","1222","1112","1211","0111","1101","1110","1011"};
			
			sword = new ArrayList<String>();
			sword.add("02");
			sword.add("10");
			sword.add("22");
			
			knife = new ArrayList<String>();
			knife.add("00");
			knife.add("20");
			
			shield = new ArrayList<String>();
			shield.add("21");
			
			minotaur = "10";
			
		} 
		else 
		{
			wall = new String[]{"1323","2313","2223","2322","2232","3222",
					               "2131","3121","1121","2111","1112",
					               "1211","1202","0212", "0001", "0100"};
			sword = new ArrayList<String>();
			sword.add("03");
			sword.add("11");
			sword.add("30");
			
			knife = new ArrayList<String>();
			knife.add("00");
			knife.add("13");
			knife.add("33");
			knife.add("21");
			
			shield = new ArrayList<String>();
			shield.add("02");
			shield.add("10");
			shield.add("22");
			shield.add("31");
			
			minotaur = "20";
		}
	}
	
	
	public String getMinotaur()
	{
		return minotaur;
	}
	
	//show maze and current position of player.
	public static void show(Player player)
	{
		for (int i=0; i<length; i++)
		{
			for (int j=0; j<length; j++)
			{				
				String position = ""+i+j;
				String content = "No objects";
				for (String e: player.maze.getSword())
				{
					if (e.equals(position)) 
						content ="sword";
				}
				for (String e: player.maze.getShield())
				{
					if (e.equals(position)) 
						content= "shield";
				}
				for (String e: player.maze.getKnife())
				{
					if (e.equals(position)) 
						content= "knife";
				}
				if (position.equals(player.maze.minotaur))
				{
					content = "minotaur";
				}
				System.out.println("(R"+i+", C"+j+"): "+content);
			}
		}	
		System.out.println("Current position: R"+player.cY+", C"+player.cX);
	}
	
	public int getLength()
	{
		return maze.length;
	}
	
	public String[] getWall()
	{
		return wall;
	}
	
	public ArrayList<String> getSword()
	{
		return sword;
	}
	
	public ArrayList<String> getKnife()
	{
		return knife;
	}
	
	public ArrayList<String> getShield()
	{
		return shield;
	}
	//method to check wall
	public boolean checkWall(String position)
	{
		for (int i =0; i < wall.length; i++)
		{
			if (position.equals(wall[i]))
			{
				return true;
			}
		}
		return false;
	}
}
