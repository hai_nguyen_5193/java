
import sheffield.*;

/**
 * Implement the Nim game
 * @author Phan Trung Hai Nguyen
 */
public class PlayNim 
{
	private Computer cp;
	private User u;
	private Nim game;
	
	/**
	 * constructor
	 */
	public PlayNim()
	{
		cp = new Computer();
		u = new User();
	}
	
	/**
	 * display the result of the game
	 * @param size the number of heaps
	 * @return "user" if user wins, "computer" if computer wins, or "exit" if user enters -1
	 */
	public String playNimResult(int size)
	{
		game = new Nim(cp, u, size);
		return game.play();
	}
}

/**
 * Will run the game
 * @author Phan Trung Hai Nguyen
 */
class Nim 
{		
	//variable to record the step user performances
	public static int step = 0;
	
	Computer cp;
	User u;
	NimState state;
	
	/**
	 * Constructor
	 * @param cp computer player
	 * @param u user
	 * @param size The number of heaps
	 */
	public Nim(Computer cp, User u, int size)
	{
		this.cp = cp;
		this.u = u;
		state = new NimState(size);
	}
	
	public String play()
	{
		state.setUp();
		int count = 1;
		while (true)
		{
			int turn = state.whoseTurn();
			if (turn==0)
			{
				//user's turn
				step++;
				System.out.println("("+count+")  USER: ");
				
				state.turn++;
				u.getPile(state);
				
				if (u.pile == -1)
				{
					return "exit";
				}
				
				u.move(state);	
				state.remove += u.object;
				
				boolean finish = state.finish();
				if (finish)
				{
					System.out.println("You won the Nim game");
					return "user";
				}
				
				state = state.copy(); 
			}
			else
			{
				//computer's turn
				System.out.println("("+count+")  COMPUTER: ");
				state.turn++;
				cp.move(state);
				state.remove += cp.object;
				
				boolean finish = state.finish();
				if (finish)
				{
					System.out.println("You losed the Nim game");
					return "computer";
				}
				state = state.copy();
			}
			count++;
		}
	}
}

/**
 * Model the state of the game
 * @author Phan Trung Hai Nguyen
 *
 */
class NimState 
{
	int turn;
	int remove;   //No. of objects removed so far
	int[][] board;
	int size;
	
	/**
	 * Constructor
	 * @param size the number of heaps
	 */
	public NimState(int size)
	{
		this.size = size;
		board = new int[size][2*size-1];
		turn = 0;
		remove = 0;
	}
	
	/**
	 * Constructor
	 * @param size the number of heaps
	 * @param board the state of the board
	 * @param turn whose turn it is
	 * @param remove the number of objects removed so far
	 */
	public NimState(int size, int[][] board, int turn, int remove)
	{
		this.size = size;
		this.board = board;
		this.turn = turn;
		this.remove = remove;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public int[][] getBoard()
	{
		return board;
	}
	
	public int getTurn()
	{
		return turn;
	}
	
	public int getRemove()
	{
		return remove;
	}
	
	/**
	 * Copy the current state of the game to another object
	 * @return a new object
	 */
	public NimState copy()
	{
		return new NimState(getSize(), getBoard(), getTurn(), getRemove());
	}
	
	//set up the board
	public void setUp()
	{
		int middle = size-1;  //the middle cell of the board
		for (int i=0; i<size; i++)
		{
			for (int j=0; j<=i; j++)
			{
				board[i][middle+j] = 1;
				board[i][middle-j] = 1;
			}
		}
	}
	
	//return whose turn it is
	public int whoseTurn()
	{
		return turn % 2;
	}
	
	//check game finish or not
	public boolean finish()
	{
		if (remove == size*size)
		{
			return true;
		}
		return false;
	}
	
	/**check particular pile empty or not
	 * @param pile
	 * @return true if pile is empty
	 */
	public boolean pileEmpty(int pile)
	{
		for (int i=0; i < 2*size-1; i++)
		{
			if (board[pile][i] == 1)
			{
				return false;
			}
		}
		return true;
	}
	
	//calculate the objects in particular pile
	public int countObject(int pile)
	{
		int count = 0;
		for (int i=0; i<2*size-1; i++)
		{
			if (board[pile][i] == 1)
			{
				count++;
			}
		}
		return count;
	}
	
	//calculate the first object's index in a pile
	public int index(int pile)
	{
		for (int i=0; i<2*size-1; i++)
		{
			if (board[pile][i] == 1)
				return i;
		}
		return -1;
	}
	
	//remove
	public void remove(int pile, int object)
	{
		int index = index(pile);
		for (int i=0; i<object; i++)
		{
			board[pile][index+i] = 0;
		}
	}
	
	//print the state of the game
	public void printBoard()
	{
		for (int i=0; i<size; i++)
		{
			System.out.print(i);
			for (int j=0; j<2*size-1; j++)
			{
				if (board[i][j]==0)
				{
					System.out.printf("%3s", " ");
				}
				else 
				{
					System.out.printf("%3s", "|");
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
}

/**
 * model the computer player (random player)
 * @author Phan Trung Hai Nguyen
 *
 */
class Computer 
{
	private int pile;
	int object;
				
	/**
	 * Constructor
	 */
	public Computer()
	{
	}
	
	public void move(NimState state)
	{
		getPile(state);
		getObject(state);
		state.remove(pile, object);	
		System.out.println("Computer took "+ object+ " object(s) from pile "+ pile);
		state.printBoard();
	}
	
	public void getPile(NimState state)
	{
		pile = (int) (100*Math.random()) % state.size;
		while (state.pileEmpty(pile))
		{
			pile = (int) (100*Math.random()) % state.size;
		}
	}
	
	public void getObject(NimState state)
	{
		object = ((int) (100*Math.random()) % state.countObject(pile))+1;
	}
	
}

/**
 * model the user player
 * @author Phan Trung Hai Nguyen
 *
 */
class User 
{
	int pile;
	int object;
	private EasyReader reader;
		
	/**
	 * Constructor
	 */
	public User()
	{
		reader = new EasyReader();
	}
	
	public void move(NimState state)
	{
		//getPile(state);
		getObject(state);
		state.remove(pile, object);
		System.out.println("User took "+ object+ " object(s) from pile "+ pile);
		state.printBoard();
	}
	
	
	public void getPile(NimState state)
	{
		pile = reader.readInt("Enter the pile: ");
		
		while ((pile != -1) && (state.pileEmpty(pile)))
		{
			System.out.println("Pile empty");
			pile = reader.readInt("Enter another pile: ");
		}
	}
	
	public void getObject(NimState state)
	{
		object = reader.readInt("Enter the objects: ");
		while (object > state.countObject(pile))
		{
			System.out.println("not enough objects to remove");
			object = reader.readInt("Enter another objects: ");
		}
	}
	
}