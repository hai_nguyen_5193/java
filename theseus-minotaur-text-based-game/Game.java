

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import sheffield.*;

/**
 * This main class will implement all classes
 */
public class Game {
	
	protected EasyReader reader;
	protected EasyWriter writer;
	
	protected StringWriter testOutput;
	
	public static int time; //time unit of the game
	private boolean win; //the status of the game
	
	//the initial position of player
	private static int iniX;
	private static int iniY;
	
	private Status status;
	private Quit quit;
	private PlayNim nim;
	private Move move;
	private Cell cell;
	private Fighting fighting;
	private Register register;
	private Administrator administrator;
	private Picking picking;
		
	/**
	 * Constructor
	 */
	 public Game()
	{
		quit = new Quit();
		status = new Status();
		nim = new PlayNim();
		time = 0;
		win = false;
		fighting = new Fighting();
		picking = new Picking();
		cell = new Cell();
		move = new Move();
		administrator = new Administrator();
		register = new Register();
				
		reader = new EasyReader();
		writer = new EasyWriter();
		
	}

	/** 
	 * Constructor 
	 */
	 /*
	public Game(String inputData)
	{
		testOutput = new StringWriter();
		writer = new EasyWriter(testOutput);
		reader = new EasyReader(new StringReader(inputData),testOutput);
	}
	
	public String getTestOutput()
	{
		if (testOutput == null)
			throw new IllegalArgumentException("Not under test, no test output available");
		
		return testOutput.toString();
	}
	*/
	/**
	 * Will control the whole system
	 */
	public void run()
	{
		String name = reader.readString("What is your name? \n> ");
	    if (name.isEmpty())
		    name = "unknown";

	    writer.println("Hello, "+name);
	    		
		while(!reader.eof())
		{
			String cmd = reader.readString("> ");		
			if (cmd.equals("quit"))   //quit
			{
				break;
			}
			
			else if (cmd.equals("name"))
			{
				writer.format("user name is %s",name);
				writer.println();
			}
			else if (cmd.contains("login"))   //login function
			{
				//administrator login
				if (cmd.equalsIgnoreCase("login administrator"))
				{
					//enter and check administrator password
					String password = reader.readString("Enter password \n> ");
					
					//wrong password
					if (!administrator.checkPassword(password))
					{
						writer.println("Incorrect Password !");
					} 
					else 
					{
						writer.println("Welcome Administrator!");
						do 
					    {
					    	System.out.print("> ");
					        cmd = reader.readString();
						    register.detectCommand(cmd);
						    System.out.print("Continue as Administrator? (y/n) \n> ");
					    }while (reader.readString().equals("y"));
				    }
				}
				
				//player login
				else if (register.checkPlayer(cmd.substring(cmd.indexOf(" ")+1)))
				{			    	
					//enter password
					System.out.print("Enter password \n> ");
					String playerPass = reader.readString();
					
					//check password
			        if (playerPass.equals(register.playerList.get(register.index).password)) 
			        {
						//player object
						Player player = register.playerList.get(register.index);
						writer.println("Welcome "+ player.getName());
						
						/**
						 * Now start the player's functions
						 * choosing a maze: two mazes (3x3 and 4x4)
						 */
						System.out.print("Select a maze (3x3 or 4x4)(type 3/4) \n>");
						int mazeSize = reader.readInt();
						
						//set the maze for the player
						player.setMaze(new Maze(mazeSize));
						
					    /**
					     * Set the current position for the player
					     * Player should start from the right side
					     * and randomly choose row.
					     */
						int size = player.maze.getLength();
						iniX = size-1;
						player.setCurrentX(iniX);
						iniY = (int)(100*Math.random()) % size;
						player.setCurrentY(iniY);
						
						//moving
						writer.println("Let's start !");
						System.out.print("> ");
						cmd = reader.readString();
						while(!reader.eof())							
						{
							String content = cell.showCell(player);
							
							//display the history of the game.
							if (cmd.equalsIgnoreCase("display"))
							{
								for (History e: History.hlist)
								{
									writer.printf("%3s %3d",e.getCmd(),e.getTime());
									writer.println();
								}
							}
							
							//quit
							else if (cmd.equalsIgnoreCase("quit"))
							{
								writer.println("Game terminated");
								quit.stop();
							}
							
							//fighting Minotaur
							else if (cmd.equalsIgnoreCase("fight"))
							{
								if (player.getPosition().equalsIgnoreCase(player.maze.minotaur))
								{
									time +=5;
									String result = fighting.fightResult(player);
									if (result.equals("win"))
									{
										win = true;
										writer.println("Congratulation ! You win");
									} 
									else 
									{
										writer.println("Bad luck ! You lose");
									}
									History.hlist.add(new History(cmd, time));
								}
								else
								{
									writer.println("Minotaur not here");
								}
							}
							
							//playing Nim
							else if (cmd.equalsIgnoreCase("play NIM"))
							{
								boolean stop = false;
								while (!stop)
								{
								    //enter the size of Nim (heaps)
								    System.out.print("Enter the heaps (3 to 5) \n> ");
								    int nimSize = reader.readInt();
								    System.out.println("Let's play a Nim game...");
								
								    String result = nim.playNimResult(nimSize);
								
								    if (result.equalsIgnoreCase("user"))
								    {
									    //increase by 2 times heap if user wins
									    player.strength += 2*nimSize;
									    stop = true;
								    } 
								
								    if (result.equalsIgnoreCase("computer"))
								    {
								    	//decrease by 5 if user loses
									    player.strength -= 5;
									    stop = true;
								    }
								
								    time += Nim.step;
								    History.hlist.add(new History(cmd, time));
								
								    //the player wants to exit Nim game
								    if (result.equalsIgnoreCase("exit"))
								    {
									    writer.println("Nim game exit !");
									    break;
								    }
								}
							}
														
							//moving in the maze
							else if (cmd.equalsIgnoreCase("move n")
									|| cmd.equalsIgnoreCase("move s")
									    || cmd.equalsIgnoreCase("move w")
									        || cmd.equalsIgnoreCase("move e"))
							{
								//moving direction
								String direction = cmd.substring(cmd.indexOf(" ")+1);
								
								switch (direction)
								{
								    case "n": 
									    move.moveN(player);
									    break;
								    case "s": 
								    	move.moveS(player);
								    	break;
								    case "w": 
									    move.moveW(player);
									    break;
								    case "e": 
									    move.moveE(player);
									    break;
								    default: 
									    writer.println("Unregconised command" + cmd);
								}
								
								writer.println("Current position: R"+player.cY + " C"+ player.cX);								
								History.hlist.add(new History(cmd, time));
								
								//check player return to entrance after win or not
								String position = player.cY +""+ player.cX;
								if ((position.equals(iniY+""+iniX)) && (win == true))
								{
									writer.println("Entrance, here you are\n Game terminated");
									quit.stop();
								}
							}
							
							//check the content of the cell
							else if (cmd.equalsIgnoreCase("show cell"))
							{
								writer.println(content +" in this cell");
								History.hlist.add(new History(cmd, time));
							}
							
							//show time elapsed and objects carried
							else if (cmd.equalsIgnoreCase("show status"))
							{
								status.show(player, cmd);
							}
							
							//show maze
							else if (cmd.equalsIgnoreCase("show maze"))
							{
								Maze.show(player);
								History.hlist.add(new History(cmd, time));
							}
							
							//picking object
							else if (cmd.equalsIgnoreCase("pickup knife")
									|| cmd.equalsIgnoreCase("pickup sword")
								    	|| cmd.equalsIgnoreCase("pickup shield"))
							{
								if ((content.equalsIgnoreCase("sword"))
										||(content.equalsIgnoreCase("shield"))
										    ||(content.equalsIgnoreCase("knife")))
								{
								    picking.pickUp(player, content);
								    History.hlist.add(new History(cmd, time));
								}
								else
								{
									writer.println("No objects to pick up");
								}
							}
							
							//dropping object
							else if (cmd.equalsIgnoreCase("drop sword")
									|| cmd.equalsIgnoreCase("drop shield")
									|| cmd.equalsIgnoreCase("drop knife"))
							{
								String object = cmd.substring(cmd.indexOf(" ")+1);
								picking.drop(player, object);
								History.hlist.add(new History(cmd, time));
							}
							else 
							{
								writer.println("received unrecognised command: "+cmd);
							}
							
							System.out.print("> ");
							cmd = reader.readString();
						}
					} 
			        else 
			        {
						writer.println("Incorrect password !");
					}
				} 
				else 
				{
			    	writer.println("received unrecognised command:"+cmd);
			    }
			}
			else
			{
				writer.println("received unrecognised command: "+cmd);
			}
		}
		writer.println("Game terminated");
	}
	
	/** The main method for the game.
	 * @param args command-line arguments
	 * @throws IOException on IO error
	 */
	public static void main(String[] args) throws IOException 
	{
		new Game().run();
	}

}
