

/**
 * model the cell of the maze
 * @author Phan Trung Hai Nguyen
 *
 */
public class Cell 
{
	
	/**
	 * constructor
	 */
	public Cell()
	{};
	
	//check the content of current cell
	public String showCell(Player player)
	{
		String position = player.getPosition();
		for (String e: player.maze.getSword())
		{
			if (e.equals(position)) 
				return "sword";
		}
		for (String e: player.maze.getShield())
		{
			if (e.equals(position)) 
				return "shield";
		}
		for (String e: player.maze.getKnife())
		{
			if (e.equals(position)) 
				return "knife";
		}
		if (position.equals(player.maze.getMinotaur()))
		{
			return "minotaur";
		}
		return "No object";
	}

}
